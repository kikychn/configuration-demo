package com.twuc.configurationdemo.controller;

import com.twuc.configurationdemo.dao.OfficeRepository;
import com.twuc.configurationdemo.model.Office;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api")
public class OfficeController {

    @Autowired
    private OfficeRepository officeRepository;

    @GetMapping("/office/{id}")
    public ResponseEntity<Office> getCity(@PathVariable Long id) {
        Office office = officeRepository.findById(id).orElseThrow(NoSuchElementException::new);
        return ResponseEntity.ok()
                .body(office);
    }

    @PostMapping("/addOffice")
    public ResponseEntity<Office> addOffice(@RequestBody String city) {
        Office savedOffice = officeRepository.save(new Office(city));
        return ResponseEntity.ok()
                .body(savedOffice);
    }
}
