package com.twuc.configurationdemo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Office {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false, length = 36)
    private String city;

    public Office() {
    }

    public Office(String city) {
        this.city = city;
    }

    public Long getId() {
        return id;
    }

    public String getCity() {
        return city;
    }
}
