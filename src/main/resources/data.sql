DROP TABLE IF EXISTS office;

CREATE TABLE office (
  id Long AUTO_INCREMENT  PRIMARY KEY,
  city VARCHAR(36) NOT NULL
);

INSERT INTO office (city) VALUES
  ('Beijing');